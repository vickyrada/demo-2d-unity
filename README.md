# README #

### INFORMACION ###

Este repositorio incluye un DEMO 2D. Prueba práctica para la empresa CETTIC.

### CONTROLES ###

Personaje
- Movimiento con flechas (arrows) del teclado.
- Barra espaciadora salto.
- Tecla ESC para salir de la aplicación.
- La Compilacion para PC se encuentra en la raiz del directorio y lleva por nombre: Demo2D y Demo2D_Data.


### VALORES EDITABLES ###

- Velocidad del personaje.
- Daño de enemigos.
- Tiempo de juego (duración).
- Vida del personaje.

### ASPECTOS ADICIONALES ###
- El arte fue utilizando imágenes de Google (Normalmente un diseñador me provee del arte cuando trabajo).
- Música en los diferentes items.
- Por cuestiones de tiempo construí el mundo con varios objetos (GameObject) en vez de utilizar Prefabs (Para el caso de monedas, enemigos, suelo, etc).
- Demuestro mi manejo con el uso de Prefabs para la vida de los jugadores.