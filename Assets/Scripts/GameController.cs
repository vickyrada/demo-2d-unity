﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {
	public int enemyDamage = 0;
	public int playerLifes = 0;
	public float time = 0;
	public Text pointsLabel, timerLabel, totalPoints, totalTime;
	public Canvas canvas;
	public GameObject startMenu, endMenu;

	private GameObject gO;
	private int score;

	/*
	 * Constructor de la clase, se ejecuta al instanciar la misma.
	 */ 
	void Start () {
		Time.timeScale = 0;
		score = 0;

		// Aca dejo la opcion de obtener los datos guardados en OnApplicationQuit el juego.
		//score = PlayerPrefs.GetInt("Coins");

		pointsLabel.text = score.ToString()+" $";
	}
	
	/*
	 * Es llamada por cada fps.
	 * Actualizo el cronometro UI.
	 * Verifico si el tiempo de juego ha finalizado.
	 */ 
	void Update () {
		time -= Time.deltaTime;
		timerLabel.text = time.ToString("0:00");

		if(Input.GetKeyDown(KeyCode.Escape)){
			Application.Quit ();
		}

		if (time <= 0.0f){
			EndOfGame();
		}
	}

	/*
	 * Muestra el menu final cuando el jugador acaba sus vidas o el tiempo finaliza antes que llegue a la meta.
	 * @totalPoints: Variable para mostrar en pantalla los puntos (Label)
	 * @totalTime: Variable para mostrar en pantalla el tiempo (Label)
	 * @endMenu: Panel UI para el menu final.
	 */ 
	void EndOfGame(){
		totalPoints.text = "Puntaje: "+ score.ToString() + " $";
		totalTime.text = "Tiempo: "+ time.ToString ("0:00") + " seg.";
		endMenu.transform.SetParent (canvas.transform, false);
		if (playerLifes > 0) {
			endMenu.transform.SetAsLastSibling ();
		} else {
			endMenu.transform.SetSiblingIndex (3);
		}
		endMenu.SetActive (true);
		Time.timeScale = 0;
	}

	/*
	 * Creacion de iconos para representar la vida del jugador.
	 * Se instancia un prefab desde la carpeta Resources.
	 */ 
	private void createHearths (){
		for (int i = 0; i<playerLifes; i++){
			gO = Instantiate (Resources.Load ("Prefabs/Lifes")) as GameObject;
			gO.transform.position = new Vector3 (gO.transform.position.x + (i*30), gO.transform.position.y, gO.transform.position.z);
			gO.transform.SetParent (canvas.transform, false);
			gO.transform.SetAsLastSibling();

		}
	}
		
	/*
	 * Funcion de deteccion de colisiones (Unity3D)
	 * De acuerdo al tipo de elemento ejecuta:
	 * -Restar vidas del jugador.
	 * -Sumar puntaje.
	 * -Finalizar partida.
	 */ 
	void OnCollisionEnter2D(Collision2D collision){
		if (playerLifes > 0) {
			if(enemyDamage > playerLifes){			
				for (int i = 4; i < canvas.transform.childCount; i++){
					Destroy (canvas.transform.GetChild(i).gameObject);
				}
				EndOfGame ();
			}else{
				if (collision.gameObject.tag == "Enemy") {
					for (int i = canvas.transform.childCount - enemyDamage; i < canvas.transform.childCount; i++){
						AudioSource.PlayClipAtPoint (collision.gameObject.GetComponent<AudioSource> ().clip, collision.transform.position);
						Destroy (canvas.transform.GetChild (i).gameObject);
					}
					playerLifes -= enemyDamage;
				}else if (collision.gameObject.name == "coin"){
					score++;
					pointsLabel.text = score.ToString()+" $";
					AudioSource.PlayClipAtPoint (collision.gameObject.GetComponent<AudioSource> ().clip, collision.transform.position);
					Destroy (collision.gameObject);
				}else if(collision.gameObject.tag == "Finish"){
					EndOfGame ();
				}
			}
		}
	}

	/*
	 * Reinicia el demo.
	 */ 
	public void RestartGame(){
		SceneManager.LoadScene ("1st_scene");
	}

	/*
	 * Inicia la partida nueva.
	 */
	public void StartGame(){
		startMenu.SetActive (false);
		Time.timeScale = 1;
		createHearths ();

	}

	/*
	 * Finaliza la aplicacion.
	 */ 
	public void ExitGame(){
		Application.Quit ();
	}

	/*
	 * Al cerrar la aplicacion almacena el puntaje acumulado por el jugador.
	 */ 
	void OnApplicationQuit(){
		PlayerPrefs.SetInt ("Coins", score);
	}
}
