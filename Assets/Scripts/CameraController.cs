﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
	/*
	 * @player Personaje principal del juego.
	 * @distance Distancia entre el jugador y la camara.
	 */
	public GameObject player;
	private Vector3 distance;

	/*
	 * Inicializa la clase.
	 */
	void Start () 
	{
		distance = transform.position - player.transform.position;
	}

	/*
	 * Se ejecuta en fps estaticos (Recomendado para UI)
	 */
	void LateUpdate () 
	{
		transform.position = player.transform.position + distance;
	}
}